# movies-released

The goal here is to have a simple way of being notified about recent
movies that have been released and movies coming out soon.

The normal setup for yagmail is required found [here](https://github.com/kootenpv/yagmail#username-and-password).
After the initial setup of username and password, you can 
schedule a cron job to email you with the movie releases.

The crontab.template file contains an example of how to set up a cron job.

Everything else is automatic.

## Install
`git clone git@bitbucket.org:FranticallySearching/movies-released.git`

## Run in the console
`python main.py -c`

## Email instead
`python main.py email1@mail.com email2@mail.net`


