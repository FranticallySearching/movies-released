import pickle
import urllib.error
import urllib.request
from collections import defaultdict
from datetime import datetime, timedelta

from bs4 import BeautifulSoup


def get_cached_movies(year: int) -> defaultdict:
    try:
        with open("movies_{year}.pickle".format(**{"year": year}), "rb") as pickle_in:
            return pickle.load(pickle_in)
    except FileNotFoundError:
        return defaultdict()


def store_movie_records(movies: dict, year: int):
    """ Pickle movies to file based on year """
    with open("movies_{year}.pickle".format(**{"year": year}), "wb") as pickle_out:
        pickle.dump(movies, pickle_out)


def scrape(year: int) -> defaultdict:
    """ :return defaultdict movies of dates to movie objects """
    url = "https://www.moviefone.com/movies/{year}/?page=1".format(**{"year": year})
    movies = defaultdict(list)
    while url:
        soup = make_soup(url)
        for section in get_movie_sections(soup):
            links = get_links(section)
            movie = Movie(title=get_title(section),
                          release_date=get_release_date(section),
                          rating=get_rating(section),
                          star_rating=get_star_rating(section),
                          trailer_link=links["trailer"],
                          movie_summary_link=links["summary"])
            movies[movie.release_date].append(movie)
        url = get_next_link(soup)
    return movies


def gen_date_range(start: datetime, end: datetime) -> iter:
    """ :return generator for datetime objects within the range of start and end dates """
    for i in range((end-start).days):
        yield end - timedelta(days=i)
    yield start


class Movie(object):
    def __init__(
            self,
            title: str,
            release_date: datetime,
            rating: str,
            star_rating: int,
            trailer_link: str,
            movie_summary_link: str):
        self._title = title
        self.release_date = release_date
        self.rating = rating
        self.star_rating = star_rating
        self.trailer_link = trailer_link
        self.movie_summary_link = movie_summary_link

    def __str__(self):
        return self.super_title + ": " + self.release_date.date().isoformat()

    @property
    def full_title(self) -> str:
        return self._title

    @property
    def super_title(self) -> str:
        return self._title.split(":", maxsplit=1)[0].strip()

    @property
    def subtitle(self) -> str:
        return self._title.split(":", maxsplit=1)[1].strip()


def get_title(tag) -> str:
    """ :return movie title """
    return tag.find("h3").getText().strip()


def get_release_date(tag) -> datetime:
    """ :return release date """
    return datetime.strptime(tag.find("span", attrs={"class": "available-text"}).getText().strip(), "%B %d, %Y")


def get_rating(tag) -> str:
    """ :return rating based on Motion Picture Association of America film rating system. Example: PG-13, R, G, etc """
    return tag.find("span", attrs={"class": "rating"}).getText().strip()


def get_star_rating(tag) -> int:
    """ :return star rating for movie. If there are currently no star ratings, return -1 """
    try:
        for attr in tag.find("div", attrs={"class": "star-rating"}).get("class"):
            if attr.startswith("stars-"):
                return int(attr[-1])
    except AttributeError:
        return -1


def get_links(tag) -> dict:
    """ :return dict containing the summary_link and the trailer_link. Defaults None """
    anchors = tag.find_all("a", attrs={"class": "button blue"})
    summary_link = trailer_link = None
    for anchor in anchors:
        if anchor.getText().strip().lower() == "more info":
            summary_link = anchor.get("href")
        elif anchor.getText().strip().lower() == "watch trailer":
            trailer_link = anchor.get("href")
    return {"summary": summary_link, "trailer": trailer_link}


def make_soup(url: str, debug: bool = True) -> BeautifulSoup:
    try:
        soup = BeautifulSoup(urllib.request.urlopen(url), "html.parser")
        [s.extract() for s in soup(['style', 'script'])]
        return soup
    except (urllib.error.HTTPError, urllib.error.URLError) as e:
        if debug is True:
            print("Unable to create soup for URL {url}".format(**{"url": url}))
        raise e


def get_next_link(soup) -> str:
    """ :return next link if there is one. Default empty string """
    try:
        return (soup.find("a", attrs={"class": "next-button"}).get("href") or "").strip()
    except AttributeError:
        return ""


def get_movie_sections(soup) -> list:
    """ :return sections containing movie data """
    return soup.find_all("div", attrs={"class": "movie-inner"})
