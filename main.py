"""
Script for emailing (or printing to console) the movies that are
coming out in the next few weeks and movies that were released in theaters
in the last couple of weeks.

Example email usage:
python main.py email1@mail.com email2@mail.net

Example console display:
python main.py -c
"""
from argparse import ArgumentParser
from datetime import datetime, timedelta

import yagmail

from scrape import gen_date_range, get_cached_movies, scrape, store_movie_records


def configure_parser():
    parser = ArgumentParser(description="Email people about new movies.")
    parser.add_argument("-c", "--console_only", required=False, action="store_true", help="Print to console flag.")
    parser.add_argument("-p", "--password", required=False, help="Override password.")
    parser.add_argument("--addresses", nargs="+", required=False, help="Format: address1@mail.net address2@mail.net")
    return parser.parse_args()


def get_today() -> datetime:
    """ :return datetime today without the timestamp """
    today = datetime.today()
    return datetime(today.year, today.month, today.day)


def gen_movies(today: datetime, past_days: int = 21, upcoming_days: int = 14, use_cache: bool = True):
    """ Print the movies that are coming out from past_days to upcoming_days """
    movies = None
    if use_cache is True:
        movies = get_cached_movies(today.year)
    if not movies:
        movies = scrape(today.year)
        store_movie_records(movies, today.year)

    for dt in gen_date_range(today - timedelta(days=past_days), today + timedelta(days=upcoming_days)):
        for m in movies.get(dt, []):
            yield m


def main():
    args = configure_parser()
    today = get_today()
    contents = "".join([str(movie) + "\n" for movie in gen_movies(today)])
    if args.console_only is True:
        print(contents)
        return

    yag = yagmail.SMTP(password=args.password) if args.password else yagmail.SMTP()
    yag.send(subject="Movie time!", contents=contents, bcc=args.addresses)
    print("Email sent on {date}".format(**{"date": today.date().isoformat()}))


if __name__ == "__main__":
    main()
