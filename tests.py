import datetime
import unittest
import urllib.error

from bs4 import BeautifulSoup

import scrape


class ScrapeTest(unittest.TestCase):
    def test_make_soup_bad_link(self):
        with self.assertRaises(urllib.error.URLError):
            scrape.make_soup("https://asdaasf.asdasda.asdawegweg.asdzghryj.adfafgaga.adfah", debug=False)

    def test_gen_date_range(self):
        dts = list(scrape.gen_date_range(datetime.datetime(2019, 1, 1), datetime.datetime(2019, 1, 5)))
        expected = [datetime.datetime(2019, 1, 5),
                    datetime.datetime(2019, 1, 4),
                    datetime.datetime(2019, 1, 3),
                    datetime.datetime(2019, 1, 2),
                    datetime.datetime(2019, 1, 1), ]
        self.assertListEqual(expected, dts)

    def test_get_cached_movies(self):
        self.assertDictEqual({}, scrape.get_cached_movies(-1))

    def test_get_release_date(self):
        soup = BeautifulSoup("""<span class="available-text"> January 1, 2019 </span>""", "html.parser")
        self.assertEqual(datetime.datetime(2019, 1, 1), scrape.get_release_date(soup))

    def test_get_release_date_bad_format(self):
        soup = BeautifulSoup("""<span class="available-text"> January 1, 19 </span>""", "html.parser")
        with self.assertRaises(ValueError):
            scrape.get_release_date(soup)

    def test_get_star_rating(self):
        soup = BeautifulSoup("""<div class="star-rating stars-3"></div>""", "html.parser")
        self.assertEqual(3, scrape.get_star_rating(soup))

    def test_get_links(self):
        html = """
        <a class="button blue" href="test summary"> MORE INFO </a>
        <a class="button red" href="skip"> OTHER SECTION </a>
        <a class="button blue" href="test trailer"> WATCH TRAILER </a>
        """
        soup = BeautifulSoup(html, "html.parser")
        self.assertDictEqual({"summary": "test summary", "trailer": "test trailer"}, scrape.get_links(soup))

    def test_get_next_link(self):
        soup = BeautifulSoup("""<a class="next-button" href=" test "></a>""", "html.parser")
        self.assertEqual("test", scrape.get_next_link(soup))

    def test_get_next_link_no_href(self):
        soup = BeautifulSoup("""<a class="next-button"></a>""", "html.parser")
        self.assertEqual("", scrape.get_next_link(soup))

    def test_get_next_link_no_next_link(self):
        self.assertEqual("", scrape.get_next_link(BeautifulSoup("", "html.parser")))

    def test_get_movie_sections(self):
        html = """
        <div class="movie-inner"></div>
        <div class="movie-inner"></div>
        <div class="movie-inner"></div>
        <div class="movie-inner"></div>
        <div class="movie-inner"></div>
        """
        self.assertEqual(5, len(scrape.get_movie_sections(BeautifulSoup(html, "html.parser"))))

    def test_get_movie_sections_no_sections(self):
        self.assertEqual(0, len(scrape.get_movie_sections(BeautifulSoup("", "html.parser"))))


class MovieTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.movie = scrape.Movie(title=" super title : subtitle: part II",
                                 release_date=datetime.datetime(2019, 1, 1),
                                 rating="PG",
                                 star_rating=3,
                                 trailer_link="trailer_link_dot_com",
                                 movie_summary_link="movie_summary_link_dot_com")

    def test_property_full_title(self):
        self.assertEqual(self.movie._title, self.movie.full_title)

    def test_property_super_title(self):
        self.assertEqual("super title", self.movie.super_title)

    def test_property_subtitle(self):
        self.assertEqual("subtitle: part II", self.movie.subtitle)

    def test___str__(self):
        self.assertEqual("super title: 2019-01-01", str(self.movie))
